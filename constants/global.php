<?php
    // List global constant for this app
    // Whitelist image extension
    define("WHITELIST_IMAGE_EXTENSION", array(
        "image/jpg",
        "image/jpeg",
        "image/gif",
        "image/svg",
        "image/png"
    ));

    // gender
    define("GENDER", array(1 => "Nam", 2 => "Nữ"));

    // Department
    define("DEPARTMENT", array("None" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu"));
    
    // Regex validate
    define("REGEX", "/^[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}$/");

    // Target dir
    define("TARGET_DIR", "upload/");

    $error = array(
        "name" => "",
        "gender" => "",
        "birthday" => "",
        "department" => "",
        "avatar" => "",
    );

    // $is_valid = True;
?>