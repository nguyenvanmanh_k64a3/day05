<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Information Form</title>
</head>
<link rel="stylesheet" type="text/css" href="styles/register_confirm.css" />
<script src="https://cdn.tailwindcss.com"></script>
<script src="https://unpkg.com/flowbite@1.5.3/dist/datepicker.js"></script>
<body>
    <?php
        include "./utils/validator.php";
        session_start();
        $error = array(
            "name" => "",
            "gender" => "",
            "birthday" => "",
            "department" => "",
            "avatar" => "",
        );
        $is_valid = True;

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $_POST["file"] = $_FILES["avatar"];
            validator_form($_POST, $error, $is_valid);

            if ($is_valid) {
                $_SESSION['post-data'] = $_POST;
                header("location: ./pages/confirm.php");
            }
            
        }        
    ?>

    <div class="w-screen h-screen flex justify-center items-center text-md">
        <div class="w-fit border-2 border-cyan-700 pt-[70px] pl-[47px] pb-[38px] pr-[35px]">
            <?php
                foreach($error as $key => $value) {
                    echo "<h3 class='text-red-500 text-xl underline decoration-pink-700'> $value </h3>";
                }
            ?>

            <form
                method="POST"
                action=''
                enctype='multipart/form-data'
                class="flex flex-col justify-around pt-[20px] h-[400px]"
            >
                <!-- Name -->
                <div class="flex h-[40px]">
                    <div 
                        class="flex h-full w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="alumnus-name" class="leading-none after:content-['*'] after:text-red-600">
                                Họ và tên
                        </label>
                    </div>
                    <div>
                        <input
                            class="ml-[20px] w-[320px] border-2 border-cyan-700 h-[40px] pl-[10px]"
                            type="text"
                            name="name"
                            id="alumnus-name"
                        >
                    </div>
                </div>

                <!-- Gender -->
                <div class="flex h-[40px]">
                    <div class="flex h-full w-[120px] justify-center items-center bg-green-500 px-[12px]">
                        <label for="gender" class="leading-none after:content-['*'] after:text-red-600">
                            Giới tính
                        </label>
                    </div>
                    <div class="flex w-[160px] justify-between ml-[20px]">
                        <div class="flex items-center">
                            <input 
                                class="appearance-none border-2 border-cyan-700 checked:bg-green-600 pl-[10px]"
                                type="radio" 
                                name="gender"
                                id="gender-male"
                                value="1"
                            >
                            <span>Nam</span>
                        </div>
                        
                        <div class="flex items-center">
                            <input
                                class="appearance-none border-2 border-cyan-700 checked:bg-green-600 pl-[10px]"
                                type="radio" 
                                name="gender"
                                id="gender-female"
                                value="2"
                            >
                            <span>Nữ</span>
                        </div>
                    </div>
                </div>

                <!-- Department -->
                <div class="flex h-[40px]">
                    <div 
                        class="flex h-full w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="department" class="leading-none after:content-['*'] after:text-red-600">
                                Phân khoa
                        </label>
                    </div>

                    <div>
                        <select id="department" name="department" class="w-[160px] ml-[20px] border-2 border-cyan-700 h-full">
                            <option value="None" selected></option>
                            <option value="MAT">Khoa học máy tính</option>
                            <option value="KDL">Khoa học dữ liệu</option>
                        </select>
                    </div>
                    
                </div>

                <!-- Birthday -->
                <div class="flex h-[40px]">
                    <div 
                        class="flex h-full w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="birthday" class="leading-none after:content-['*'] after:text-red-600">
                                Ngày sinh
                        </label>
                    </div>
                    <div>
                        <input
                            datepicker
                            class="ml-[20px] w-[160px] border-2 border-cyan-700 h-[40px] pl-[10px]"
                            type="text"
                            name="birthday"
                            id="birthday"
                            datepicker-format="dd/mm/yyyy"
                            placeholder="dd/mm/yyyy"
                        >
                    </div>
                </div>

                <!-- Address -->
                <div class="flex h-[40px]">
                    <div 
                        class="flex h-full w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="address" class="leading-none after:content-['*'] after:text-red-600">
                                Địa chỉ
                        </label>
                    </div>
                    <div>
                        <input
                            class="ml-[20px] w-[320px] border-2 border-cyan-700 h-[40px] pl-[10px]"
                            type="text"
                            name="address"
                            id="address"
                        >
                    </div>
                </div>
            
                <!-- Image -->
                <div class="flex h-[40px]">
                    <div 
                        class="flex h-full w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="address" class="leading-none after:content-['*'] after:text-red-600">
                                Hình ảnh
                        </label>
                    </div>
                    <div class="flex items-center justify-center">
                        <input
                            class="ml-[20px]"
                            type="file"
                            name="avatar"
                            id="avatar"
                        >
                    </div>
                </div>

                <!-- Button Submit -->
                <div class="flex justify-center items-center pt-[30px]">
                    <button class="w-[120px] h-[48px] bg-green-500 rounded-md border-2 border-cyan-700" type="submit">
                        Đăng ký
                    </button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>