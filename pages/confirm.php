<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Information Form</title>
</head>
<link rel="stylesheet" href="styles/register_confirm.css">
<script src="https://cdn.tailwindcss.com"></script>
<script src="https://unpkg.com/flowbite@1.5.3/dist/datepicker.js"></script>
<body>

    <?php
        include "../constants/global.php";
        session_start();
        $alumnus_data = $_SESSION["post-data"];
    ?>

    <div class="w-screen h-screen flex justify-center items-center text-md">
        <div class="w-fit border-2 border-cyan-700 pt-[70px] pl-[47px] pb-[38px] pr-[35px]">

            <form
                method="POST"
                action=''
                class="pt-[20px] min-h-[400px] min-w-[460px]"
            >
                <!-- Name -->
                <div class="flex mb-[10px]">
                    <div 
                        class="flex h-[40px] w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="alumnus-name" class="leading-none after:content-['*'] after:text-red-600">
                                Họ và tên
                        </label>
                    </div>
                    <div class="flex items-center justify-center ml-[80px]">
                        <span>
                            <?php echo $alumnus_data["name"]; ?>
                        </span>
                    </div>
                </div>

                <!-- Gender -->
                <div class="flex mb-[10px]">
                    <div class="flex h-[40px] w-[120px] justify-center items-center bg-green-500 px-[12px]">
                        <label for="gender" class="leading-none after:content-['*'] after:text-red-600">
                            Giới tính
                        </label>
                    </div>
                    <div class="flex items-center justify-center ml-[80px]">
                            <span>
                                <?php 
                                    $gender_id = $alumnus_data["gender"];
                                    echo GENDER[$gender_id]
                                ?>
                            </span>
                    </div>
                </div>

                <!-- Department -->
                <div class="flex mb-[10px]">
                    <div 
                        class="flex h-[40px] w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="department" class="leading-none after:content-['*'] after:text-red-600">
                                Phân khoa
                        </label>
                    </div>

                    <div class="flex items-center justify-center ml-[80px]">
                            <span>
                                <?php
                                    $alumnus_id = $alumnus_data["department"];
                                    echo DEPARTMENT[$alumnus_id];
                                ?>
                            </span>
                    </div>
                    
                </div>

                <!-- Birthday -->
                <div class="flex mb-[10px]">
                    <div 
                        class="flex h-[40px] w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="birthday" class="leading-none after:content-['*'] after:text-red-600">
                                Ngày sinh
                        </label>
                    </div>
                    <div class="flex items-center justify-center ml-[80px]">
                        <span>
                            <?php echo $alumnus_data["birthday"]; ?>
                        </span>
                    </div>
                </div>

                <!-- Address -->
                <div class="flex mb-[10px]">
                    <div 
                        class="flex h-[40px] w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="address" class="leading-none after:content-['*'] after:text-red-600">
                                Địa chỉ
                        </label>
                    </div>
                    <div class="flex items-center justify-center ml-[80px]">
                        <span>
                            <?php echo $alumnus_data["address"]; ?>
                        </span>
                    </div>
                </div>
            
                <!-- Image -->
                <div class="flex min-h-[40px] mb-[10px]">
                    <div 
                        class="flex h-[40px] w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="avatar" class="leading-none after:content-['*'] after:text-red-600">
                                Hình ảnh
                        </label>
                    </div>
                    <div class="flex items-start justify-center max-w-[35%] object-contain ml-[80px]">
                        <!-- Data here -->
                        <?php 
                        if (!empty($alumnus_data["avatar"])){
                            ?>
                                <img src="<?php echo $alumnus_data["avatar"] ?>" alt="">
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <!-- Button Submit -->
                <div class="flex justify-center items-center pt-[30px]">
                    <button class="w-[120px] h-[48px] bg-green-500 rounded-md border-2 border-cyan-700" type="submit">
                        Xác nhận
                    </button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>