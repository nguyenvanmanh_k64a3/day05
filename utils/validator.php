<?php
    include "./constants/global.php";
    
    function validator_form($form_data, &$error, &$is_valid) {
        if (empty($form_data["name"])) {
            $error["name"] = "Hãy nhập tên.";
            $is_valid = False;
        }

        if (empty($form_data["gender"])) {
            $error["gender"] = "Hãy chọn giới tính.";
            $is_valid = False;
        } 

        if ($form_data["department"] == "None") {
            $error["department"] = "Hãy chọn phân khoa.";
            $is_valid = False;
        }
        
        if (empty($form_data["birthday"])) {
            $error["birthday"] = "Hãy nhập ngày sinh.";
            $is_valid = False;
        } elseif (!preg_match(REGEX,$form_data["birthday"])) {
            $error["birthday"] = "Hãy nhập ngày sinh đúng định dạng.";
            $is_valid = False;
        }

        if ($form_data["file"]["size"] > 0) {
            $file_info = finfo_open(FILEINFO_MIME_TYPE);
            $file_mime_type = finfo_file($file_info, $form_data["file"]["tmp_name"]);

            if (!in_array($file_mime_type, WHITELIST_IMAGE_EXTENSION)) {
                $error["avatar"] = "Hãy upload ảnh đúng định dạng!";
                $is_valid = False;
            } else {
                if (!is_dir(TARGET_DIR)) {
                    mkdir(TARGET_DIR, 0777, true);
                }

                $detail = explode(".", $form_data["file"]["name"]);
                $filename = $detail[0] . "_" . date('YmdHis') . "." . $detail[1];

                $file_save_path = TARGET_DIR . basename($filename);

                if (move_uploaded_file($form_data["file"]["tmp_name"], $file_save_path)) {
                    $_POST["avatar"] = "../".TARGET_DIR.$filename;
                }
            }
        }
    }
?>